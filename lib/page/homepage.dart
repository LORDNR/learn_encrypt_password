import 'package:flutter/material.dart';
import 'package:flutter_string_encryption/flutter_string_encryption.dart';
// import 'package:flutter_string_encryption/flutter_string_encryption.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  TextEditingController pass = TextEditingController();

  var key = 'null';
  late String encrypt, decrypt;

  var password = 'null';

  var encryptedS = 'null';

  late PlatformStringCryptor cryptor;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Container(
        child: ListView(
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
              child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(
                      height: 200,
                    ),
                    TextField(
                      controller: pass,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: 'Password',
                        hintText: 'กรุณากรอก Password',
                      ),
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(0, 15, 0, 15),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          ButtonTheme(
                            minWidth: 200.0,
                            height: 50.0,
                            buttonColor: Colors.green,
                            child: RaisedButton(
                              onPressed: () {
                                Encrypt();
                              },
                              child: Text(
                                "Encrypt",
                                style: TextStyle(
                                    fontSize: 24, color: Colors.white),
                              ),
                            ),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(18.0)),
                          ),
                          SizedBox(
                            height: 30,
                          ),
                          ButtonTheme(
                            minWidth: 150.0,
                            height: 50.0,
                            buttonColor: Colors.red,
                            child: RaisedButton(
                              onPressed: () {
                                Decrypt();
                              },
                              child: Text(
                                "Decrypt",
                                style: TextStyle(
                                    fontSize: 24, color: Colors.white),
                              ),
                            ),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(18.0)),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void Encrypt() async {
    cryptor = PlatformStringCryptor();
    final salt = await cryptor.generateSalt();

    password = pass.text;

    key = await cryptor.generateKeyFromPassword(password, salt);

    encryptedS = await cryptor.encrypt(password, key);

    print(encryptedS);
  }

  void Decrypt() async {
    try {
      var decryptedS = await cryptor.decrypt(encryptedS, key);

      print(decryptedS);
    } on MacMismatchException {}
  }
}
